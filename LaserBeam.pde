class LaserBeam extends GraphicObject {
  boolean alive;
  boolean inFluid;
  PVector size;
  float mass;
  float density;


  LaserBeam(PVector centre, float longueurCanon, float angle) {
    density = 1000;
    alive = true;
    size = new PVector(width/32, width/32 );
    mass = (PI*sq(size.x)) * density;
    position = new PVector (centre.x + cos(radians(angle)) * longueurCanon, centre.y + sin(radians(angle)) * longueurCanon); 
    acceleration = new PVector ( cos(radians(angle)) * (CANON_SHOOTING_VELOCITY/PIXEL_TO_METER), sin(radians(angle)) * (CANON_SHOOTING_VELOCITY/PIXEL_TO_METER));
    velocity = new PVector(0, 0);
  }

  PVector getPosition() {
    return position;
  }
  float getRadius() {
    return size.x;
  }
  boolean is_alive() {
    return alive;
  }
  void kill() {
    alive=false;
  }

  void applyAcceleration(PVector AccelerationAjoute) {
    acceleration.add(AccelerationAjoute);
  }

  void applyForce(PVector force) {
    PVector finalForce = force.copy();
    finalForce.div(mass);
    acceleration.add(finalForce);
  }


  void update(float deltaTime) {
    velocity.add(acceleration);
    position.add(velocity);
    position.add(PVector.mult(velocity, deltaTime/FRAME_DELTA));
    acceleration.mult(0);
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(atan(velocity.y/velocity.x));
    fill (#F3FC00);
    ellipse(0, 0, size.x *1.0, size.y *0.05);
    
    popMatrix();
    
  }
}
