class Vaisseau extends GraphicObject {

  PVector position;
  PVector velocity;
  PVector acceleration;

  float angle;
  float r = 10;
  float rotationSpeed;

  float sizeVaisseau;
  boolean alive;


  float power;
  Vaisseau() {
    power =0;
    position = new PVector(width/2, height/2);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    rotationSpeed = 0;

    sizeVaisseau = 20;
    VaisseauIsAlive = true;
  }

  void update(float deltaTime) {
    angle += rotationSpeed;
    //rotationSpeed = rotationSpeed *0;
    acceleration.x = cos(radians(angle)) * power;
    acceleration.y = sin(radians(angle)) * power;
    
    if (keyCode != UP) {
      power = power *0;
    }
    keyCode = SHIFT;

    if (angle > 360) {
      angle=0;
    } else if (angle <0) {
      angle=360;
    }

    velocity.add(acceleration);
    position.add(velocity);

    if (velocity.mag()>MAX_SPEED_SHUTTLE){
      
    }


    acceleration.mult(0);
  }

  void display() {

    if (!VaisseauIsAlive) {
      EndOfGame();
    }

    pushMatrix();
    translate (position.x, position.y);
    rotate(radians(angle-90));



    stroke(#0319FF);
    fill(#F5FA00);

    beginShape();
    /*
    vertex(0*RATIO_VAISSEAU, 10*RATIO_VAISSEAU);
    vertex(-10*RATIO_VAISSEAU, -5*RATIO_VAISSEAU);
    vertex(-5*RATIO_VAISSEAU, -5*RATIO_VAISSEAU);
    vertex(-5*RATIO_VAISSEAU, -10*RATIO_VAISSEAU);
    vertex(5*RATIO_VAISSEAU, -10*RATIO_VAISSEAU);
    vertex(5*RATIO_VAISSEAU, -5*RATIO_VAISSEAU);
    vertex(10*RATIO_VAISSEAU, -5*RATIO_VAISSEAU);
    vertex(0*RATIO_VAISSEAU, 10*RATIO_VAISSEAU);
    endShape();*/
    
    vertex(0*RATIO_VAISSEAU,12*RATIO_VAISSEAU);
    vertex(1*RATIO_VAISSEAU,6*RATIO_VAISSEAU);
    vertex(1*RATIO_VAISSEAU,3*RATIO_VAISSEAU);
    vertex(2*RATIO_VAISSEAU,1*RATIO_VAISSEAU);
    vertex(4*RATIO_VAISSEAU,1*RATIO_VAISSEAU);
    vertex(5*RATIO_VAISSEAU,4*RATIO_VAISSEAU);
    vertex(6*RATIO_VAISSEAU,1*RATIO_VAISSEAU);
    vertex(6*RATIO_VAISSEAU,-3*RATIO_VAISSEAU);
    vertex(5*RATIO_VAISSEAU,-4*RATIO_VAISSEAU);
    vertex(4*RATIO_VAISSEAU,-3*RATIO_VAISSEAU);
    vertex(4*RATIO_VAISSEAU,0*RATIO_VAISSEAU);
    vertex(2*RATIO_VAISSEAU,0*RATIO_VAISSEAU);
    vertex(2*RATIO_VAISSEAU,-11*RATIO_VAISSEAU);
    vertex(1*RATIO_VAISSEAU,-11*RATIO_VAISSEAU);
    vertex(1*RATIO_VAISSEAU,-12*RATIO_VAISSEAU);
    vertex(0*RATIO_VAISSEAU,-12*RATIO_VAISSEAU);

    vertex(0*RATIO_VAISSEAU,-12*RATIO_VAISSEAU);
    vertex(-1*RATIO_VAISSEAU,-12*RATIO_VAISSEAU);
    vertex(-1*RATIO_VAISSEAU,-11*RATIO_VAISSEAU);
    vertex(-2*RATIO_VAISSEAU,-11*RATIO_VAISSEAU);
    vertex(-2*RATIO_VAISSEAU,0*RATIO_VAISSEAU);
    vertex(-4*RATIO_VAISSEAU,0*RATIO_VAISSEAU);
    vertex(-4*RATIO_VAISSEAU,-3*RATIO_VAISSEAU);
    vertex(-5*RATIO_VAISSEAU,-4*RATIO_VAISSEAU);
    vertex(-6*RATIO_VAISSEAU,-3*RATIO_VAISSEAU);
    vertex(-6*RATIO_VAISSEAU,1*RATIO_VAISSEAU);
    vertex(-5*RATIO_VAISSEAU,4*RATIO_VAISSEAU);
    vertex(-4*RATIO_VAISSEAU,1*RATIO_VAISSEAU);
    vertex(-2*RATIO_VAISSEAU,1*RATIO_VAISSEAU);
    vertex(-1*RATIO_VAISSEAU,3*RATIO_VAISSEAU);
    vertex(-1*RATIO_VAISSEAU,6*RATIO_VAISSEAU);
    vertex(0*RATIO_VAISSEAU,12*RATIO_VAISSEAU);
    


    endShape();
    
    
    popMatrix();
  }




  PVector getPosition() {
    return position;
  }

  boolean is_alive() {
    return alive;
  };
  void kill() {
    alive=false;
  }

  boolean getVaisseauIsAlive() {
    return VaisseauIsAlive;
  }

  void setVaisseauIsAlive(boolean VAlive) {
    VaisseauIsAlive = VAlive;
  }

  void checkEdges() { 
    if (position.x > width) {
      position.x = 0;
    } else if (position.x < 0) {
      position.x = width;
    } 
    if (position.y>height) {
      position.y = 0;
    } else if (position.y <0) {
      position.y = height;
    }
  }




  void goRight() {
    rotationSpeed = 2;
    if (rotationSpeed>2) {
      rotationSpeed =2;
    }
  }

  void goLeft() {
    rotationSpeed = -2;
    if (rotationSpeed<-2) {
      rotationSpeed =-2;
    }
  }

  void goSpeed() {
    power = 0.075;
  }

  void offRotation() {
    rotationSpeed =0;
  }

  void EndOfGame() {
  }
}
