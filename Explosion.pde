class Explosion {
 
  float x;
  float y;
  
  float velX; // speed or velocity
  float velY;
  
  
  Explosion (PVector positionStart) {
   //x and y position to be in middle of screen

  x = positionStart.x;
  y = positionStart.y;

    //x = width/2;
    //y = height/2;
    
    velX = random (-5,5);
    velY = random (-5,5);

  
  }
  
  void update () {
    
    x+=velX;
    y+=velY;
    
    fill (#FEFF05);
    ellipse (x,y,3,3);
  }
  
  
}
