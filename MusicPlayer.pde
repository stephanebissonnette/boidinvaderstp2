import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;

import static javax.swing.JOptionPane.*;

class MusicPlayer{
  
  
  String musicPath;
  
  String fileNames[];
  File dir; 
  File [] files;
  
  Minim minim;
  AudioPlayer[] songs;
  FFT[] fft;
  
  int currentSong;
  int[] order;
  
  
  boolean playing;
  boolean repeat;
  
  int currentGain;
  
  float[] frequencies;
  
  
  
  MusicPlayer(PApplet app, String npath){
    
    musicPath = npath;
    
    minim = new Minim(app);
    
    loadSongs();
    
    
    reorderTracks();
    
  }
  
  float[] getFrequencies(){return frequencies;}
  
  
  void update(){
    
    if(currentSong >= 0 && currentSong < songs.length && songs.length > 0){
      
      if(!songs[order[currentSong]].isPlaying() && playing){
        songs[order[currentSong]].cue(0);
        if(!repeat){
          currentSong = (currentSong+1)%songs.length;
          songs[order[currentSong]].cue(0);
        }
        songs[order[currentSong]].play();
      }
      
      fft[order[currentSong]].forward(songs[order[currentSong]].mix);
      
      
      int timeSize = fft[order[currentSong]].timeSize();
      int ratio = 5;
      
      float lowest = 10000;
      float highest = 0;
      
      frequencies = new float[timeSize];
      
      for(float i = 0; i < timeSize; i++){
        
        float freq = (i/ratio)/(timeSize) * songs[order[currentSong]].sampleRate();
        
        float offset = (sqrt(fft[order[currentSong]].getFreq(freq)) * ((currentGain + 25)/5)) * (width/200) / 0.3;
        
        frequencies[floor(i)] = sqrt(fft[order[currentSong]].getFreq(freq));
        if(freq < lowest){
          lowest = freq;
        }
        if(freq > highest){
          highest = freq;
        }
        
        
        
      }
      
      //println(lowest, highest);
      
    }
    
  }
  
  
  
  
  
  void reorderTracks(){
    
    for(int i = 0; i < order.length; i++){
      order[i] = i;
    }
    
  }
  
  void shuffleTracks(){
    
    if(songs.length > 0){
      if(songs[order[currentSong]].isPlaying()){
        songs[order[currentSong]].pause();
      }
    }
    
    for(int i = 0; i < order.length*3; i++){
      
      int index1 = int(random(order.length));
      int index2 = int(random(order.length));
      while(index2 == index1){
        index2 = int(random(order.length));
      }
      
      int temp = order[index1];
      order[index1] = order[index2];
      order[index2] = temp;
      
    }
    
  }
  
  void getFileNames(){
    
    //if(musicPath.length() <= 0){
    //  musicPath = "D:/Audio_Songs";
    //}
    
    dir = new File(dataPath(musicPath));
    files = dir.listFiles();
    
    
    int validFileAmount = 0;
    
    for (int i = 0; i <= files.length - 1; i++){
      
      String path = files[i].getAbsolutePath();
      
      //path.toLowerCase().endsWith(".wav") || 
      
      if (path.toLowerCase().endsWith(".mp3")){
        validFileAmount++;
      }
      
    }
    
    
    fileNames = new String[validFileAmount];
    
    int currentIndex = 0;
    
    for (int i = 0; i <= files.length - 1; i++){
      
      String path = files[i].getAbsolutePath();
      String fileName = " ";
      int pathIndex = 0;
      
      //path.toLowerCase().endsWith(".wav") || 
      
      if (path.toLowerCase().endsWith(".mp3")){
        
        for(int j = 0; j < path.length(); j++){
          if(path.charAt(j) == '\\'){
            pathIndex = j;
          }
        }
        
        fileName = "";
        
        for(int j = pathIndex + 1; j < path.length(); j++){
          fileName += path.charAt(j);
        }
        
        fileNames[currentIndex] = fileName;
        
        //println(fileName);
        
        currentIndex++;
        
      }
    } 
    
  }
  
  
  void loadSongs(){
    
    getFileNames();
    
    
    songs = new AudioPlayer[fileNames.length];
    order = new int[songs.length];
    fft = new FFT[fileNames.length];
    
    if(musicPath.length() > 0){
      musicPath += '/';
    }
    
    float totalHours = 0;
    float totalMinutes = 0;
    float totalSeconds = 0;
    
    for(int i = 0; i < fileNames.length; i++){
      
      songs[i] = minim.loadFile(musicPath + fileNames[i]);
      
      float duration = float(songs[order[currentSong]].getMetaData().length());
      float seconds = floor(duration/1000);
      float minutes = floor(seconds)/60;
      seconds -= minutes * 60;
      
      totalMinutes += minutes;
      totalSeconds += seconds;
      
      while(totalSeconds >= 60){
        totalMinutes++;
        totalSeconds -= 60;
      }
      
      while(totalMinutes >= 60){
        totalHours++;
        totalMinutes -= 60;
      }
      
      
      fft[i] = new FFT(songs[i].bufferSize(), songs[i].sampleRate());
      
    }
    
    
    println("Total play time: " + nf(totalHours, 2, 0) + ":" + nf(totalMinutes, 2, 0) + ":" + nf(totalSeconds, 2, 0));
    
  }
  
  
  void togglePause(){
    
    playing = !playing;
      
    if(playing){
      songs[order[currentSong]].play();
    }else {
      songs[order[currentSong]].pause();
    }
      
  }
  
  void control(char k){
    
    if(k == ' '){
      
      //PLAY/PAUSE
      
      togglePause();
      
    }else if(k == 'r'){
      
      //LOOP
      
      repeat = !repeat;
      //println("Repeat: " + repeat);
      
      
    }else if(k == 'f'){
      
      //REWIND
      
      songs[order[currentSong]].rewind();
      
    }else if(k == 'e'){
      
      //FAST FOWARD 10 SECONDS
      
      int pos = songs[order[currentSong]].position() + 10000;
      
      if(pos > songs[order[currentSong]].length()){
        pos = 0;
        songs[order[currentSong]].cue(0);
        songs[order[currentSong]].pause();
        currentSong++;
        songs[order[currentSong]].cue(0);
        songs[order[currentSong]].play();
      }else {
        
        songs[order[currentSong]].cue(pos);
        
      }
      
    }else if(k == 'd'){
      
      //REWIND 10 SECONDS
      
      int pos = songs[order[currentSong]].position() - 10000;
      
      if(pos < 0){
        pos = 0;
      }
      
      songs[order[currentSong]].cue(pos);
      
      
    }else if(k == 'q'){
      
      //SUFFLE
      
      shuffleTracks();
      
    }else if(k == 'a'){
      
      //REORDER
      
      reorderTracks();
      
    }else if(k == CODED){
      if(keyCode == RIGHT){
        
        if(songs[order[currentSong]].isPlaying()){
          songs[order[currentSong]].pause();
        }
        
        currentSong = (currentSong+1)%songs.length;
        
        if(playing){
          songs[order[currentSong]].play();
        }
        
      }else if(keyCode == LEFT){
        
        if(songs[order[currentSong]].isPlaying()){
          songs[order[currentSong]].pause();
        }
        
        currentSong--;
        if(currentSong < 0){
          currentSong = songs.length - 1;
        }
        
        if(playing){
          songs[order[currentSong]].play();
        }
        
      }else if(keyCode == DOWN){
        
        int pastGain = currentGain;
        
        if(currentGain > -20){
          currentGain--;
        }
        
        for(int i = 0; i < songs.length; i++){
          songs[i].shiftGain(pastGain, currentGain, 500);
        }
        
      }else if(keyCode == UP){
        
        int pastGain = currentGain;
        
        if(currentGain < 6){
          currentGain++;
        }
        
        for(int i = 0; i < songs.length; i++){
          songs[i].shiftGain(pastGain, currentGain, 500);
        }
        
      }
    }
    
  }
  
  
  
}
