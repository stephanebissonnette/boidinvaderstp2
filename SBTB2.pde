
float currentTime;
float previousTime;
float deltaTime;
float targetTime;
Vaisseau vaisseau;
ArrayList<Mover> flock;
ArrayList<LaserBeam> laserBeam;
ArrayList<Explosion> smallExplosions;
int NumberOfFlock = 50;
int NumberOfLaserBeam = 5;
MusicPlayer myMusicPlayer;

boolean debug = false;
boolean firstPress = false;
boolean separate = true;
int score;
boolean gameOver = false;
int shakeCounter;
int shakeMax;
Explosion [] explosion = new Explosion [50];
boolean nouvelleExplosion;
boolean pewpew;
boolean proutprout;

void setup () {
  fullScreen(P2D);
  //size (800, 600);
  noCursor();
  currentTime = millis();
  previousTime = millis();
  targetTime = millis();
  vaisseau = new Vaisseau();
  flock = new ArrayList<Mover>();
  laserBeam = new ArrayList<LaserBeam>();
  smallExplosions = new ArrayList<Explosion>();
  pewpew = false;
  proutprout = false;

  vaisseau.VaisseauIsAlive = true;
  score =0;
  shakeCounter = 0;

  shakeMax =300;
  smooth ();
  nouvelleExplosion = true;

  for (int i = 0; i < NumberOfFlock; i++) {
    float angleRandom = random (0, 360);
    float distanceMin = random(width/3, height/2);
    Mover m = new Mover(new PVector(cos(angleRandom) * distanceMin, sin(angleRandom)* distanceMin  ), new PVector(random (-5, 5), random(-5, 5)));// random a 1/3 distance d'écran du vaisseau
    //    Mover m = new Mover(new PVector(random(0, width), random(0, height)), new PVector(random (-2, 2), random(-2, 2)));// random nimporte ou
    m.fillColor = color(random(0), random(0), random(255));
    flock.add(m);
  }
  flock.get(0).debug = true;
  
 // myMusicPlayer = new MusicPlayer(this, "data");
  
}





void draw () {
  background(0);
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;
  update(deltaTime);
  display();
}


void update(float delta) {
  vaisseau.checkEdges();
  vaisseau.update(deltaTime);

  for (int i = flock.size() -1; i>=0; i--) {
    Mover part = flock.get(i);
    part.applyForce(part.seek(vaisseau.getPosition()).mult(0.3));
    part.flock(flock);
    part.update(delta);
    part.ifColide(vaisseau);
    part.ifBeam(laserBeam);

    if (part.hit) {
      // println("ON DETRUIT LE BOIDS NUMÉRO: ", i);
      // particules explosion


      smallExplosions.add(new Explosion(part.location));


      for (Explosion sE : smallExplosions) {   
        for (int j=0; j<explosion.length; j++) {
          explosion [j] = new Explosion(part.location);
        }
      }
      flock.remove(i);
    }
  }


  for (int j=0; j<explosion.length; j++) {
    if (explosion[j] != null) {
      explosion[j].update();
    }
  }

  if (pewpew) {
    if (currentTime - targetTime > FIRE_DELAY) {
      targetTime = currentTime;
      if (laserBeam.size()<5) {           
        if (vaisseau.getVaisseauIsAlive()) {
          laserBeam.add(new LaserBeam(new PVector(vaisseau.position.x, vaisseau.position.y), 10, vaisseau.angle));
        }
      }
    }
  }


  for (int i = laserBeam.size() -1; i>=0; i--) {
    LaserBeam laser = laserBeam.get(i);

    if ((laser.position.x > width)||(laser.position.x < 0)||(laser.position.y > height) || (laser.position.y <0))
    {
      laserBeam.remove(i);
    }
  }

  for (LaserBeam l : laserBeam) {
    l.update(delta);
  }

  if (proutprout) {
    vaisseau.goSpeed();
  }

  if (!vaisseau.getVaisseauIsAlive()) {

    if (nouvelleExplosion) {   
      // particules explosion
      for (int i=0; i<explosion.length; i++) {
        explosion [i] = new Explosion (vaisseau.position);
      }
      nouvelleExplosion = false;
    }

    for (int i=0; i<explosion.length; i++) {
      explosion[i].update();
    }

    if (shakeCounter < shakeMax) {
      float shakeAmount1 = random(-(shakeMax-shakeCounter), shakeMax-shakeCounter)/25;
      float shakeAmount2 = random(-(shakeMax-shakeCounter), shakeMax-shakeCounter)/25;
      pushMatrix();
      translate(shakeAmount1, shakeAmount2);
      display();
      popMatrix();
      shakeCounter ++;
    } else {

      Reinitialize();
    }
    //ps.addParticle();
    //ps.run();
  }
}






void Reinitialize() {
  // reinit game quand shake terminé
  //*********************************************************
  vaisseau = new Vaisseau();
  flock = new ArrayList<Mover>();
  for (int i = 0; i < NumberOfFlock; i++) {
    float angleRandom = random (0, 360);
    float distanceMin = random(width/3, height/2);
    Mover m = new Mover(new PVector(cos(angleRandom) * distanceMin, sin(angleRandom)* distanceMin  ), new PVector(random (-5, 5), random(-5, 5)));// random a 1/3 distance d'écran du vaisseau
    //    Mover m = new Mover(new PVector(random(0, width), random(0, height)), new PVector(random (-2, 2), random(-2, 2)));// random nimporte ou
    m.fillColor = color(random(0), random(0), random(255));
    flock.add(m);
  }
  laserBeam = new ArrayList<LaserBeam>();
  vaisseau.VaisseauIsAlive = true;
  score =0;
  shakeCounter = 0;
  //*********************************************************
}


void display () {




  // Vaisseau
  if (vaisseau.getVaisseauIsAlive()) {
    vaisseau.display();
  };

  // Ennemys
  for (Mover m : flock) {
    m.display();
  }

  for (LaserBeam l : laserBeam) {
    l.display();
  }


  textSize(height / 30);
  fill(255);
  text(score+"  Alive:"+flock.size(), width / 32, height /10);


  // LE VAISSEAU EST DÉTRUIT
  if (!vaisseau.getVaisseauIsAlive()) {
    textSize(96);
    fill(255);
    String gameoverTxt = "GAME OVER";
    text(gameoverTxt, width / 2 - textWidth(gameoverTxt)/2, height /2);
  }

  // LES ENNEMIS SONT TOUS DÉTRUITS
  if (flock.size()==0) {
    textSize(96);
    fill(#F6FF03);
    String gameoverTxt = "YOU WIN !!!";
    text(gameoverTxt, width / 2 - textWidth(gameoverTxt)/2, height /2);

    textSize(48);
    fill(#030CFF);
    gameoverTxt = "R to Reinitialize";
    text(gameoverTxt, width / 2 - textWidth(gameoverTxt)/2, height /1.5);
  }
}



void keyPressed() {
  if (key == 'a' || key == 'A') {
    vaisseau.goLeft();
  } else if (key == 'd'|| key == 'D') {
    vaisseau.goRight();
  } else if (key == 'w'|| key == 'W') {
    proutprout=true;
  } else if (key == 'b'|| key == 'B') {
    flock.get(0).debug = !flock.get(0).debug;
  } else if (key == 's'|| key == 'S') {
    separate = !separate;
    for (Mover m : flock) {   
      m.weightSeparation =separate ? 2.0: 0;
    }
  } else  if (key == ' ') {
  //  myMusicPlayer.songs["laser.mp3"].play();
  //  songs[order[currentSong]].play();
    pewpew = true;
  } else if (key == 'r'|| key == 'R') {
    Reinitialize();
  }
}// fin void keyPressed


void keyReleased() {

  if (key == 'a' || key == 'A') {
    vaisseau.offRotation();
  } else if (key == 'd' || key == 'D') {
    vaisseau.offRotation();
  } else if (key == 'w'|| key == 'W') {
    proutprout = false;
  } else if (key == ' ') {
    pewpew=false;
  }
}
void mousePressed() {

  if (firstPress) {
    firstPress = false;   
    //float angleRandom = random (0, 360);
    //float distanceMin = random(width/3, height/2);
    //Mover m = new Mover(new PVector(cos(angleRandom) * distanceMin, sin(angleRandom)* distanceMin  ), new PVector(random (-5, 5), random(-5, 5)));
    Mover m = new Mover(new PVector(mouseX, mouseY), new PVector(random (-5, 5), random(-5, 5)));
    m.fillColor = color(random(50, 255), 0, 0);
    flock.add(m);
  }
}

void mouseReleased() {

  firstPress = true;
}


void starsGenerator(){

}
